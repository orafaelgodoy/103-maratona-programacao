package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Equipe {
	public int id;
	public List<Aluno> alunos = new ArrayList<>();

	@Override
	public String toString() {
		String texto = "";
		texto += "ID = " + id + "\n";

		for (Aluno aluno : alunos) {
			texto += aluno.nome;
			texto += "\n";

		}
		return texto;
	}

}
