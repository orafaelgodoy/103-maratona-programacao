package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Gerador {
	
	public static ArrayList<Equipe> sortear(List<Aluno> alunos) {
		
		ArrayList<Equipe> equipes = new ArrayList<>();
		int contId = 1;
		while (alunos.size() > 0) {
			Equipe equipe = new Equipe();
			for(int i = 0; i < 3; i++) {
				int sorteio = (int) Math.floor(Math.random() * alunos.size());
				Aluno aluno = alunos.remove(sorteio);
				equipe.alunos.add(aluno);
			}
			equipe.id = contId;
			equipes.add(equipe);
			
			contId ++;
	
		}
		return null;
	}
	

}
